//
// Created by 214 on 05.09.2018.
//

#include "PhrasesContainer.h"

auto cmp = [](std::pair<std::string, int> const &a, std::pair<std::string, int> const &b) {
    return a.second != b.second ? a.second > b.second : a.first < b.first;
};

void PhrasesContainer::insert(std::string &s) {
    container[s]++;
}

void PhrasesContainer::print(int border, std::ostream &outputStream) {
    for (auto &i : container)
        if (i.second >= border)
            outputStream << i.first << "(" << i.second << ")\n";
}

void PhrasesContainer::printSorted(int border, std::ostream &outputStream) {
    std::vector<std::pair<std::string, int>> v(container.begin(), container.end());
    sort(v.begin(), v.end(), cmp);
    for (auto &i : v)
        if (i.second >= border)
            outputStream << i.first << "(" << i.second << ")\n";
    v.clear();
}
