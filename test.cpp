#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file

#include "catch.hpp"

#include "count_phrases.h"
#include "parameters.h"

TEST_CASE("Test C1", "countPhrases") {
    std::stringstream out;
    std::stringstream in(        "In the town where I was born\n"
                                 "Lived a man who sailed to sea\n"
                                 "And he told us of his life\n"
                                 "In the land of submarines");
    countPhrases(2, 2, in, out);
    REQUIRE (out.str() == "In the (2)\n");
}

TEST_CASE("Test C2", "countPhrases") {
    std::stringstream out;
    std::stringstream in(    "we all live in a yellow submarine\n"
                             "yellow submarine yellow submarine\n"
                             "we all live in a yellow submarine\n"
                             "yellow submarine yellow submarine");

    countPhrases(3, 4, in, out);
    REQUIRE (out.str() == "submarine yellow submarine (4)\nyellow submarine yellow (4)\n");
}

TEST_CASE("Test C3", "countPhrases") {
    std::stringstream out;
    std::stringstream in("aaa bb c");

    countPhrases(4, 4, in, out);
    REQUIRE (out.str() == "");
}

TEST_CASE("Test C4", "countPhrases") {
    std::stringstream out;
    std::stringstream in("one two two three three three");

    countPhrases(1, 1, in, out);
    REQUIRE (out.str() == "three (3)\ntwo (2)\none (1)\n");
}

TEST_CASE("Test IB1", "InputBuffer") {
    InputBuffer buff;
    std::string s1, s2, s3;
    s1 = "Hello";
    s2 = "World!";
    buff.push(s1);
    buff.push(s2);
    s3 = buff.get_phrase();
    REQUIRE (s3 == "Hello World! ");
}

TEST_CASE("Test PC1", "PhrasesContainer") {
    PhrasesContainer cont;
    std::stringstream out;
    std::string s1, s2, s3;

    //"a b c b c b c b c"
    s1 = "a b ";
    s2 = "b c ";
    s3 = "c b ";

    cont.insert(s1);
    for (int i = 0; i < 4; i++)
        cont.insert(s2);
    for (int i = 0; i < 3; i++)
        cont.insert(s3);

    cont.print(3, out);

    REQUIRE (out.str() == "b c (4)\nc b (3)\n");
}

TEST_CASE("Test P1", "parameters") {
    int wordsNum = 2, printBorder = 2;
    std::string fileName;

    int argc = 6;
    auto **argv = new char *[argc];

    argv[0] = const_cast<char *>("C:\\01_phrases.exe");
    argv[1] = const_cast<char *>("-m");
    argv[2] = const_cast<char *>("8");
    argv[3] = const_cast<char *>("-n");
    argv[4] = const_cast<char *>("4");
    argv[5] = const_cast<char *>("input.txt");

    getOptions(argc, argv, wordsNum, printBorder, fileName);

    REQUIRE (wordsNum == 4);
    REQUIRE (printBorder == 8);
    REQUIRE (fileName == "input.txt");

    delete[] argv;
}

TEST_CASE("Test P2", "parameters") {
    int wordsNum = 2, printBorder = 2;
    std::string fileName;

    int argc = 4;
    auto **argv = new char *[argc];

    argv[0] = const_cast<char *>("C:\\01_phrases.exe");
    argv[1] = const_cast<char *>("-n");
    argv[2] = const_cast<char *>("3");
    argv[3] = const_cast<char *>("-");

    getOptions(argc, argv, wordsNum, printBorder, fileName);

    REQUIRE (wordsNum == 3);
    REQUIRE (printBorder == 2);
    REQUIRE (fileName == "-");

    delete[] argv;
}

TEST_CASE("Test P3", "parameters") {
    std::stringstream out;
    int wordsNum = 2, printBorder = 2;
    std::string fileName;

    int argc = 3;
    auto **argv = new char *[argc];

    argv[0] = const_cast<char *>("C:\\01_phrases.exe");
    argv[1] = const_cast<char *>("-m");
    argv[2] = const_cast<char *>("-9");

    getOptions(argc, argv, wordsNum, printBorder, fileName, out);

    REQUIRE (out.str() == "Error! Wrong arguments.\n");

    delete[] argv;
}

TEST_CASE("Test P4", "parameters") {
    std::stringstream out;
    int wordsNum = 2, printBorder = 2;
    std::string fileName;

    int argc = 5;
    auto **argv = new char *[argc];

    argv[0] = const_cast<char *>("C:\\01_phrases.exe");
    argv[1] = const_cast<char *>("-m");
    argv[2] = const_cast<char *>("5");
    argv[3] = const_cast<char *>("-");
    argv[4] = const_cast<char *>("asdfghj");

    getOptions(argc, argv, wordsNum, printBorder, fileName, out);

    REQUIRE (out.str() == "Error! Wrong arguments.\n");

    delete[] argv;
}