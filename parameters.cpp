//
// Created by 214 on 05.09.2018.
//

#include "parameters.h"

int getOptions(int argc, char **argv, int &wordsNum, int &printBorder, std::string &fileName,
               std::ostream &errorStream) {

    for (int i = 1; i < argc; i++) {
        std::string opt = argv[i];
        if (opt == "-n") {
            int n = strtol(argv[++i], nullptr, 10);

            if (n > 0)
                wordsNum = n;
            else {
                errorStream << "Error! Wrong arguments.\n";
                return -1;
            }

        } else if (opt == "-m") {
            int m = strtol(argv[++i], nullptr, 10);

            if (m > 0)
                printBorder = m;
            else {
                errorStream << "Error! Wrong arguments.\n";
                return -1;
            }

        } else if (fileName.empty()) {
            fileName = opt;
        } else {
            errorStream << "Error! Wrong arguments.\n";
            return -1;
        }
    }
}