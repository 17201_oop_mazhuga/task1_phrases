cmake_minimum_required(VERSION 3.1)
project(01_phrases)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES PhrasesContainer.cpp
        PhrasesContainer.h
        InputBuffer.cpp
        InputBuffer.h
        parameters.cpp
        count_phrases.cpp
        count_phrases.h
        parameters.h)
add_executable(01_phrases main.cpp ${SOURCE_FILES})
add_executable(unittest test.cpp ${SOURCE_FILES})