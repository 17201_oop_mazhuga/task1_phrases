#include <fstream>

#include "parameters.h"
#include "count_phrases.h"

int main(int argc, char **argv) {

    int wordsNum = 2, printBorder = 2;
    std::string fileName;

    if (getOptions(argc, argv, wordsNum, printBorder, fileName) == -1)
        exit(1);

    if (!fileName.empty() && fileName != "-") {
        std::ifstream inputFile;
        inputFile.open(fileName);
        if (inputFile.good())
            countPhrases(wordsNum, printBorder, inputFile);
        else {
            std::cerr << "Error. Cannot open file.";
            exit(1);
        }
    } else
        countPhrases(wordsNum, printBorder, std::cin);

    return 0;
}