//
// Created by 214 on 05.09.2018.
//

#include <iostream>

#include "PhrasesContainer.h"
#include "InputBuffer.h"

#ifndef INC_01_PHRASES_COUNT_PHRASES_H
#define INC_01_PHRASES_COUNT_PHRASES_H

int countPhrases(int, int, std::istream &, std::ostream & = std::cout);

#endif //INC_01_PHRASES_COUNT_PHRASES_H