//
// Created by 214 on 05.09.2018.
//

#include "count_phrases.h"

int countPhrases(int wordsNum, int printBorder, std::istream &inputStream, std::ostream &outputStream) {
    PhrasesContainer cont;
    InputBuffer buff(wordsNum);

    std::string s;

    for (int j = 0; j < wordsNum - 1 && inputStream.good(); j++) {
        inputStream >> s;
        buff.push(s);
    }

    while (inputStream.good()) {
        inputStream >> s;
        buff.push(s);

        s = buff.get_phrase();
        buff.delete_first();

        cont.insert(s);
    }

    cont.printSorted (printBorder, outputStream);
}