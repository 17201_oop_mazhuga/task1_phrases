//
// Created by 214 on 05.09.2018.
//

#include "InputBuffer.h"

InputBuffer::InputBuffer(int wnum) : wordsNum(wnum) {};

void InputBuffer::push(std::string &str) {
    buffer.push_back(str);
}

void InputBuffer::delete_first() {
    buffer.erase(buffer.begin());
};

std::string InputBuffer::get_phrase() {
    std::string s;

    for (int i = 0; i < wordsNum; i++)
        s += buffer[i] + " ";

    return s;
}