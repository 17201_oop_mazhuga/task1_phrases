//
// Created by 214 on 05.09.2018.
//
#include <iostream>
#include <map>
#include <vector>
#include <algorithm>

#ifndef INC_01_PHRASES_PHRASESCONTAINER_H
#define INC_01_PHRASES_PHRASESCONTAINER_H

class PhrasesContainer {
private:
    std::map<std::string, unsigned int> container;
public:
    void insert(std::string &);
    void print(int = 1, std::ostream & = std::cout);
    void printSorted(int = 1, std::ostream & = std::cout);
};


#endif //INC_01_PHRASES_PHRASESCONTAINER_H
