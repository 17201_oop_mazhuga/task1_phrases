//
// Created by 214 on 05.09.2018.
//

#include <iostream>
#include <vector>

#ifndef INC_01_PHRASES_INPUTBUFFER_H
#define INC_01_PHRASES_INPUTBUFFER_H

class InputBuffer {
private:
    int wordsNum;
    std::vector <std::string> buffer;
public:
    InputBuffer(int = 2);
    void push(std::string &);
    void delete_first();
    std::string get_phrase();
};

#endif //INC_01_PHRASES_INPUTBUFFER_H
