Task 1: Phrases

-n (number) - number of words in one phrase (default = 2)

-m (number) - number of insertions of a phrase to be printed (default = 1)